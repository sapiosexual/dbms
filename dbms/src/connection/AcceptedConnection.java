package connection;

import dbms.QueryProcessor;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author Admin
 */
public class AcceptedConnection extends Thread {
    private static final int PACK_SIZE = 16 * 1024;
    static final int INIT_BUF_SIZE = 8;
    
    Socket s;
    int num;
    InputStream in;
    OutputStream out;
    PrintWriter pout;
    StringReader sin;
    
    public AcceptedConnection(int num, Socket s) {
        // копируем данные
        this.num = num;
        this.s = s;
        try {
            in = s.getInputStream();
            out = s.getOutputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // и запускаем новый вычислительный поток (см. ф-ю run())
        setDaemon(true);
        setPriority(NORM_PRIORITY);
    }

    @Override
    public void run() {
        try {
            System.out.println("Connection accepted " + num );
            while(true) {
                // создаём строку, содержащую полученную от клиента информацию
                String data = readAll();
                String result = "";
                if(data.equals("Error")) {
                    out.write(StringPacker.pack("Transfer error"));
                    out.flush();
                } else {
                    result = QueryProcessor.getInstance().process(data);
                    out.write(StringPacker.pack(result));
                    out.flush();
                }
                
                System.out.println(""+num+": length: " + data.getBytes().length);
                if(result.length() < 50 && !result.isEmpty()) {
                    System.out.println(""+num+": sent: " + result);
                }
                if(result.equals("Unknown")) {
                    System.out.println(""+num+": recived: " + data);
                }
                
            }
        }
        catch(Exception e) {
            System.out.println("init error: "+e); // вывод исключений
            e.printStackTrace();
        }
    }
    
    private String readAll() throws Exception{
        String res = "";
        int length = 0;
        int checksum = 0;
        try {
            byte[] buffer = new byte[INIT_BUF_SIZE];
            in.read(buffer);
            ByteBuffer buf = ByteBuffer.wrap(buffer);
            buf.order(ByteOrder.LITTLE_ENDIAN);
            length = buf.getInt();
            checksum = buf.getInt();
            while (length > 0) {
                buffer = new byte[PACK_SIZE];
                int read = in.read(buffer);
                res += new String(buffer).trim();
                length -= read;
            }
            if(checksum != StringPacker.checksum(res)) {
                res = "Error";
                in.skip(in.available());
            }
        } catch(Exception e) {
            System.out.println("read error: "+e);
            throw e;
        }
        return res;
    }
}
