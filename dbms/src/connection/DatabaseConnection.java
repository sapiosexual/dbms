package connection;

import dbms.AttributeType;
import dbms.DataBase;
import dbms.Record;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Migration class
 * @author Admin
 */
public class DatabaseConnection {
    public static DatabaseConnection dbc = new DatabaseConnection();
    Connection connection;
    public static int LIMIT = 100000;
    
    public DatabaseConnection() {
        try {
            connection = DriverManager.getConnection(
               "jdbc:postgresql://localhost/papers", "postgres", "1");
            connection.setAutoCommit(false);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }
    
    static public Map<String, AttributeType> paperAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("id", AttributeType.INTEGER);
        attributes.put("title", AttributeType.STRING);
        attributes.put("link", AttributeType.STRING);
        attributes.put("abstract", AttributeType.STRING);
        attributes.put("doi", AttributeType.STRING);
        attributes.put("research_area_id", AttributeType.INTEGER);
        attributes.put("publication_id", AttributeType.INTEGER);
        attributes.put("spage", AttributeType.STRING);
        attributes.put("fpage", AttributeType.STRING);
        return attributes;
    }
    
    static public List<Record> getPaper(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = paperAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM paper "
                    + "WHERE id < 1100000 "
                    + "ORDER BY id "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("id", rs.getInt("id"));
                rec.setComparable("title", rs.getString("title"));
                rec.setComparable("link", rs.getString("link"));
                rec.setComparable("abstract", rs.getString("abstract"));
                rec.setComparable("doi", rs.getString("doi"));
                rec.setComparable("research_area_id", rs.getInt("research_area_id"));
                rec.setComparable("publication_id", rs.getInt("publication_id"));
                rec.setComparable("spage", rs.getString("spage"));
                rec.setComparable("fpage", rs.getString("fpage"));
                res.add(rec);
            }
            
        } catch(Exception e) {
            e.printStackTrace();
        }
        return res;
    }
    
    static public Map<String, AttributeType> publicationAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("id", AttributeType.INTEGER);
        attributes.put("issn", AttributeType.STRING);
        attributes.put("name", AttributeType.STRING);
        attributes.put("year", AttributeType.INTEGER);
        attributes.put("publisher_id", AttributeType.INTEGER);
        attributes.put("isbn", AttributeType.STRING);
        attributes.put("type", AttributeType.STRING);
        attributes.put("volume", AttributeType.STRING);
        attributes.put("issue", AttributeType.STRING);
        return attributes;
    }
    
    static public List<Record> getPublication(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = publicationAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM publication "
                    + "WHERE id IN (SELECT publication_id "
                                 + "FROM paper "
                                 + "WHERE id < 1100000) "
                    + "ORDER BY id "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("id", rs.getInt("id"));
                rec.setComparable("issn", rs.getString("issn"));
                rec.setComparable("name", rs.getString("name"));
                rec.setComparable("year", rs.getInt("year"));
                rec.setComparable("publisher_id", rs.getInt("publisher_id"));
                rec.setComparable("isbn", rs.getString("isbn"));
                rec.setComparable("type", rs.getString("type"));
                rec.setComparable("volume", rs.getString("volume"));
                rec.setComparable("issue", rs.getString("issue"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> affiliationAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("id", AttributeType.INTEGER);
        attributes.put("name", AttributeType.STRING);
        return attributes;
    }
    
    static public List<Record> getAffiliation(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = affiliationAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM affiliation "
                    + "WHERE id IN (SELECT affiliation_id "
                                 + "FROM paper_affiliations "
                                 + "WHERE paper_id < 1100000) "
                    + "ORDER BY id "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("id", rs.getInt("id"));
                rec.setComparable("name", rs.getString("name"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> paperAffiliationsAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("paper_id", AttributeType.INTEGER);
        attributes.put("affiliation_id", AttributeType.INTEGER);
        return attributes;
    }
    
    static public List<Record> getPaperAffiliations(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = paperAffiliationsAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM paper_affiliations "
                    + "WHERE paper_id < 1100000 "
                    + "ORDER BY paper_id, affiliation_id "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("paper_id", rs.getInt("paper_id"));
                rec.setComparable("affiliation_id", rs.getInt("affiliation_id"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> authorAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("id", AttributeType.INTEGER);
        attributes.put("name", AttributeType.STRING);
        return attributes;
    }
    
    static public List<Record> getAuthor(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = authorAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM author "
                    + "WHERE id IN (SELECT author_id "
                                 + "FROM paper_authors "
                                 + "WHERE paper_id < 1100000) "
                    + "ORDER BY id "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("id", rs.getInt("id"));
                rec.setComparable("name", rs.getString("name"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> paperAuthorsAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("paper_id", AttributeType.INTEGER);
        attributes.put("author_id", AttributeType.INTEGER);
        attributes.put("position", AttributeType.INTEGER);
        return attributes;
    }
    
    static public List<Record> getPaperAuthors(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = paperAuthorsAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM paper_authors "
                    + "WHERE paper_id < 1100000 "
                    + "ORDER BY paper_id, author_id "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("paper_id", rs.getInt("paper_id"));
                rec.setComparable("author_id", rs.getInt("author_id"));
                rec.setComparable("position", rs.getInt("position"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> keyWordAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("id", AttributeType.INTEGER);
        attributes.put("term", AttributeType.STRING);
        return attributes;
    }
    
    static public List<Record> getKeyWord(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = keyWordAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM key_word "
                    + "WHERE id IN (SELECT key_word_id "
                                 + "FROM paper_key_words "
                                 + "WHERE paper_id < 1100000) "
                    + "ORDER BY id "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("id", rs.getInt("id"));
                rec.setComparable("term", rs.getString("term"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> paperKeyWordsAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("paper_id", AttributeType.INTEGER);
        attributes.put("key_word_id", AttributeType.INTEGER);
        return attributes;
    }
    
    static public List<Record> getPaperKeyWords(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = paperKeyWordsAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM paper_key_words "
                    + "WHERE paper_id < 1100000 "
                    + "ORDER BY paper_id, key_word_id "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("paper_id", rs.getInt("paper_id"));
                rec.setComparable("key_word_id", rs.getInt("key_word_id"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> publisherAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("id", AttributeType.INTEGER);
        attributes.put("name", AttributeType.STRING);
        return attributes;
    }
    
    static public List<Record> getPublisher(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = publisherAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM publisher "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("id", rs.getInt("id"));
                rec.setComparable("name", rs.getString("name"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> researchAreaAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("id", AttributeType.INTEGER);
        attributes.put("area", AttributeType.STRING);
        return attributes;
    }
    
    static public List<Record> getResearchArea(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = researchAreaAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM research_area "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("id", rs.getInt("id"));
                rec.setComparable("area", rs.getString("area"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public Map<String, AttributeType> userAttr() {
        Map<String, AttributeType> attributes = new HashMap<>();
        attributes.put("login", AttributeType.STRING);
        attributes.put("hash", AttributeType.STRING);
        attributes.put("type", AttributeType.STRING);
        attributes.put("internal_id", AttributeType.INTEGER);
        return attributes;
    }
    
    static public List<Record> getUser(int offset) {
        List<Record> res = new ArrayList<>();
        Map<String, AttributeType> attributes = userAttr();
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT * FROM \"user\" "
                    + "LIMIT ? OFFSET ?");
            cs.setInt(1, LIMIT);
            cs.setInt(2, offset);
            ResultSet rs = cs.executeQuery();
            
            while(rs.next()) {
                Record rec = new Record(attributes);
                rec.setComparable("login", rs.getString("login"));
                rec.setComparable("hash", rs.getString("hash"));
                rec.setComparable("type", rs.getString("type"));
                rec.setComparable("internal_id", rs.getInt("internal_id"));
                res.add(rec);
            }
            
        } catch(Exception e) {
        }
        return res;
    }
    
    static public int getPaperCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM paper ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getPublicationCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM publication "
                    + "WHERE id IN (SELECT publication_id "
                                 + "FROM paper "
                                 + "WHERE id < 1100000) ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getPublisherCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM publisher ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getAffiliationCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM affiliation "
                    + "WHERE id IN (SELECT affiliation_id "
                                 + "FROM paper_affiliations "
                                 + "WHERE paper_id < 1100000) ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getPaperAffiliationsCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM paper_affiliations "
                    + "WHERE paper_id < 1100000");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getAuthorCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM author "
                    + "WHERE id IN (SELECT author_id "
                                 + "FROM paper_authors "
                                 + "WHERE paper_id < 1100000) ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getPaperAuthorsCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM paper_authors "
                    + "WHERE paper_id < 1100000 ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getKeyWordCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM key_word "
                    + "WHERE id IN (SELECT key_word_id "
                                 + "FROM paper_key_words "
                                 + "WHERE paper_id < 1100000) ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getPaperKeyWordsCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM paper_key_words "
                    + "WHERE paper_id < 1100000 ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getUserCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM user ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    static public int getResearchAreaCount() {
        try {
            CallableStatement cs = dbc.connection.prepareCall(
                    "SELECT count(*) FROM research_area ");
            ResultSet rs = cs.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch(Exception e) {
        }
        return 0;
    }
    
    public static void migrate() throws IOException {
        if(true) return;
        HashSet<Integer> ids = new HashSet<>();
        for(int i = 0; i < 11; i++) {
            List<Record> recs = getPaper(i * DatabaseConnection.LIMIT);
            for(Record rec : recs) {
                if(rec.size() < DataBase.PAGE_BYTE_SIZE - 4) {
                    DataBase.getInstance().insert(rec, "paper");
                }
            }
            System.out.println("Paper: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        int count = getPublicationCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getPublication(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "publication");
            }
            System.out.println("Publication: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getPublisherCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getPublisher(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "publisher");
            }
            System.out.println("Publisher: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getKeyWordCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getKeyWord(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "key_word");
            }
            System.out.println("Key word: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getPaperKeyWordsCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getPaperKeyWords(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "paper_key_words");
            }
            System.out.println("Paper key words: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getAuthorCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = DatabaseConnection.getAuthor(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "author");
            }
            System.out.println("Author: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getPaperAuthorsCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getPaperAuthors(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "paper_authors");
            }
            System.out.println("Paper authors: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getAffiliationCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getAffiliation(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "affiliation");
            }
            System.out.println("Affiliation: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getPaperAffiliationsCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getPaperAffiliations(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "paper_affiliations");
            }
            System.out.println("Paper affiliations: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getResearchAreaCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getResearchArea(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "research_area");
            }
            System.out.println("Research area: " + (i + 1) * DatabaseConnection.LIMIT);
        }
        count = getUserCount()/DatabaseConnection.LIMIT + 1;
        for(int i = 0; i < count; i++) {
            List<Record> recs = getUser(i * DatabaseConnection.LIMIT);
            if(recs.isEmpty()) {
                System.out.println("WARNING!!!");
            }
            for(Record rec : recs) {
                DataBase.getInstance().insert(rec, "user");
            }
            System.out.println("User: " + (i + 1) * DatabaseConnection.LIMIT);
        }
    }
    
}
