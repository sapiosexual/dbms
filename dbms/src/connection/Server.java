package connection;

import java.net.InetAddress;
import java.net.ServerSocket;

/**
 *
 * @author Admin
 */
public class Server extends Thread {

    @Override
    public void run() {
        try {
            int i = 0; // счётчик подключений

            // привинтить сокет на локалхост, порт 8888
            ServerSocket server = new ServerSocket(8888, 0,
                    InetAddress.getByName("localhost"));

            System.out.println("server is started");

            // слушаем порт
            while(true)
            {
                // ждём нового подключения, после чего запускаем обработку клиента
                // в новый вычислительный поток и увеличиваем счётчик на единичку
                new AcceptedConnection(i, server.accept()).start();
                i++;
            }
        }
        catch(Exception e) {
            System.out.println("init error: "+e); // вывод исключений
        }
    }
}
