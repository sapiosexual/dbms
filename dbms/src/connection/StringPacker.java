package connection;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author Admin
 */
public class StringPacker {
    public static byte[] pack(String s) {
        long time = System.currentTimeMillis();
        ByteBuffer buf = ByteBuffer.allocate(8 + s.getBytes().length);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        buf.putInt(s.getBytes().length);
        buf.putInt(checksum(s));
        buf.put(s.getBytes());
        System.out.println("Pack time: " + ((System.currentTimeMillis() - time)/1000));
        return buf.array();
    }
    
    public static int checksum(String s) {
        int checksum = 0;
        for(char c : s.toCharArray()) {
            checksum += c;
        }
        return checksum;
    }
}
