package dbms;

/**
 *
 * @author Admin
 */
public enum AttributeType {
    INTEGER((byte)2),
    STRING((byte)1);

    private final byte type;
    private AttributeType(byte type) {
        this.type = type;
    }

    public byte getType() {
        return type;
    }
    
    public static AttributeType getType(byte type) {
        switch(type) {
            case 1:
                return STRING;
            case 2:
                return INTEGER;
            default:
                return null;
        }
    }
}
