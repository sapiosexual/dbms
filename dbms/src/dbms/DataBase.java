package dbms;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import relationalalgebra.TupleSet;

/**
 * Created by Asima on 22.11.2015.
 */
public class DataBase {
    
    private static DataBase instance;

    final int HEADER_PAGE_ID = 0;
    static RandomAccessFile raf;
    final public static int PAGE_BYTE_SIZE = 4096; //SIze of pageNumber with metadata
    final static int NULL_REF = -1;
    public static int newPagePointer = 1;

    //===========All the metadata==============
    Map<String, Table> tables = new HashMap<>();
    List<HeaderPage> headers = new ArrayList<>();

    public DataBase(String path) throws IOException {
        File file = new File(path);
        if (!file.exists()) {
            file.createNewFile();
        }
        raf = new RandomAccessFile(file, "rw");
        if (file.length() == 0) {
            initDb();
        } else {
            loadMeta();
            newPagePointer = (int) file.length() / PAGE_BYTE_SIZE + 1;
        }
    }
    
    public static DataBase getInstance() {
        if(instance == null) {
            try {
                instance = new DataBase("db.in");
            } catch (IOException e) {
                
            }
        }
        return instance;
    }

    private void initDb() {
        HashMap<String, AttributeType> attr = new HashMap<>();
        attr.put("id", AttributeType.INTEGER);
        attr.put("title", AttributeType.STRING);
        attr.put("link", AttributeType.STRING);
        attr.put("abstract", AttributeType.STRING);
        attr.put("doi", AttributeType.STRING);
        attr.put("research_area_id", AttributeType.INTEGER);
        attr.put("publication_id", AttributeType.INTEGER);
        attr.put("spage", AttributeType.STRING);
        attr.put("fpage", AttributeType.STRING);
        int dirRef = getNextPageId();
        DirectoryPage dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        Table table = new Table("paper", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("id", AttributeType.INTEGER);
        attr.put("issn", AttributeType.STRING);
        attr.put("name", AttributeType.STRING);
        attr.put("year", AttributeType.INTEGER);
        attr.put("publisher_id", AttributeType.INTEGER);
        attr.put("isbn", AttributeType.STRING);
        attr.put("type", AttributeType.STRING);
        attr.put("volume", AttributeType.STRING);
        attr.put("issue", AttributeType.STRING);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("publication", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("id", AttributeType.INTEGER);
        attr.put("name", AttributeType.STRING);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("publisher", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("id", AttributeType.INTEGER);
        attr.put("name", AttributeType.STRING);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("author", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("paper_id", AttributeType.INTEGER);
        attr.put("author_id", AttributeType.INTEGER);
        attr.put("position", AttributeType.INTEGER);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("paper_authors", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("id", AttributeType.INTEGER);
        attr.put("name", AttributeType.STRING);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("affiliation", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("paper_id", AttributeType.INTEGER);
        attr.put("affiliation_id", AttributeType.INTEGER);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("paper_affiliations", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("id", AttributeType.INTEGER);
        attr.put("term", AttributeType.STRING);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("key_word", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("paper_id", AttributeType.INTEGER);
        attr.put("key_word_id", AttributeType.INTEGER);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("paper_key_words", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("id", AttributeType.INTEGER);
        attr.put("area", AttributeType.STRING);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("research_area", dirRef, attr);
        tables.put(table.name, table);

        attr = new HashMap<>();
        attr.put("login", AttributeType.STRING);
        attr.put("hash", AttributeType.STRING);
        attr.put("type", AttributeType.STRING);
        attr.put("internal_id", AttributeType.INTEGER);
        dirRef = getNextPageId();
        dPage = new DirectoryPage(dirRef);
        try {
            dPage.saveDataOnDisk();
        } catch (Exception e) {

        }
        table = new Table("user", dirRef, attr);
        tables.put(table.name, table);

        try {
            saveMeta();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadMeta() throws IOException {
        int nextPage = HEADER_PAGE_ID;
        do {
            HeaderPage headerPage = new HeaderPage(nextPage);
            headers.add(headerPage);
            byte[] data = headerPage.loadDataFromDisk();
            ByteBuffer bb = ByteBuffer.wrap(data);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            String tableName = parseString(bb);
            while (!tableName.isEmpty() && bb.position() < PAGE_BYTE_SIZE - 4) {
                int dirPage = bb.getInt();
                Table table = new Table(tableName, dirPage);
                byte nofAttr = bb.get();
                for (int i = 0; i < nofAttr; i++) {
                    table.getAttributes().put(parseString(bb), AttributeType.getType(bb.get()));
                }
                tables.put(tableName, table);
                tableName = parseString(bb);
            }
            bb.position(PAGE_BYTE_SIZE - 4);
            nextPage = bb.getInt();
        } while (nextPage != NULL_REF);
    }

    private String parseString(ByteBuffer bb) {
        int length = bb.getShort();
        byte[] stringBytes = new byte[length];
        bb.get(stringBytes);
        return new String(stringBytes);
    }

    public void saveMeta() throws IOException {
        List<HeaderPage> newHeaders = new ArrayList<>();
        ByteBuffer bb = ByteBuffer.allocate(PAGE_BYTE_SIZE);
        HeaderPage currPage = new HeaderPage(HEADER_PAGE_ID);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        int pageCount = 0;
        for (Table tab : tables.values()) {
            byte[] meta = tab.getMetaData();
            if (meta.length > bb.capacity() - bb.position() - 4) {
                int nextPageRef;
                if (headers.size() > pageCount) {
                    nextPageRef = headers.get(pageCount++).pageID;
                } else {
                    nextPageRef = getNextPageId();
                }
                bb.position(PAGE_BYTE_SIZE - 4);
                bb.putInt(nextPageRef);
                currPage.saveDataOnDisk(bb.array());
                newHeaders.add(currPage);
                bb = ByteBuffer.allocate(PAGE_BYTE_SIZE);
                bb.order(ByteOrder.LITTLE_ENDIAN);
                currPage = new HeaderPage(nextPageRef);
            }
            bb.put(meta);
        }
        bb.position(PAGE_BYTE_SIZE - 4);
        bb.putInt(NULL_REF);
        currPage.saveDataOnDisk(bb.array());
        newHeaders.add(currPage);
        headers = newHeaders;
    }

    public void insert(Record rec, String tableName) throws IOException {
        if (!tables.containsKey(tableName)) {
            throw new IllegalArgumentException("There is no such table");
        }
        tables.get(tableName).insert(rec);
    }

    public void delete(Rid rid, String tableName) throws IOException {
        if (!tables.containsKey(tableName)) {
            throw new IllegalArgumentException("There is no such table");
        }
        tables.get(tableName).delete(rid);
    }

    public void update(Rid rid, Record newRec, String tableName) throws IOException {
        if (!tables.containsKey(tableName)) {
            throw new IllegalArgumentException("There is no such table");
        }
        tables.get(tableName).update(rid, newRec);
    }
    
    public TupleSet getRecords(String tableName) throws IOException {
        if (!tables.containsKey(tableName)) {
            throw new IllegalArgumentException("There is no such table");
        }
        return tables.get(tableName).getRecords();
    }

    public static int getNextPageId() {
        return newPagePointer++;
    }
}
