package dbms;

import connection.DatabaseConnection;
import connection.Server;
import java.io.*;

/**
 * @author Admin
 */
public class Dbms {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        DataBase.getInstance();
        Server server = new Server();
        server.start();
    }
}
