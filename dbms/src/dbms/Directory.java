package dbms;

import java.io.IOException;
import java.util.HashMap;
import java.util.TreeSet;

public class Directory {

    int dirRef;
    TreeSet<InfoPage> spaceIndex = new TreeSet<>();
    HashMap<Integer, InfoPage> pageIdIndex = new HashMap<>();
    HashMap<Integer, DirectoryPage> pages = new HashMap<>();
    DirectoryPage currPage;

    public Directory(int dirRef) {
        this.dirRef = dirRef;
        try {
            while (dirRef != DataBase.NULL_REF) {
                currPage = new DirectoryPage(dirRef);
                currPage.loadDataFromDisk();
                currPage.getPages().stream().forEach((page) -> {
                    spaceIndex.add(page);
                    pageIdIndex.put(page.pageID, page);
                });
                pages.put(dirRef, currPage);
                dirRef = currPage.nextPageID;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateDirectory(int i) throws IOException {
        pages.get(i).saveDataOnDisk();
    }

    public void startReindex(InfoPage page) {
        spaceIndex.remove(page);
    }

    public void endReindex(InfoPage page) throws IOException {
        spaceIndex.add(page);
        pages.get(page.directoryID).saveDataOnDisk();
    }

    public InfoPage getMostSuitablePage(int size) {
        InfoPage page = new InfoPage((short) (size + 2));
        page = spaceIndex.ceiling(page);
        if (page == null) {
            checkDirectoryPageSize();
            page = new InfoPage(DataBase.getNextPageId(), currPage.pageID);
            currPage.getPages().add(page);
            pageIdIndex.put(page.pageID, page);
        }
        return page;
    }

    public void checkDirectoryPageSize() {
        if (currPage.getPages().size() >= DirectoryPage.MAX_NUMBER_OF_PAGES) {
            DirectoryPage newPage = new DirectoryPage(DataBase.getNextPageId());
            currPage.nextPageID = newPage.pageID;
            try {
                currPage.saveDataOnDisk();
                currPage = newPage;
                pages.put(currPage.pageID, currPage);
                currPage.saveDataOnDisk();
            } catch (Exception e) {

            }
        }
    }

    public InfoPage getPage(int pageID) {
        return pageIdIndex.get(pageID);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(spaceIndex.size()).append("\n");
        spaceIndex.stream().forEach((page) -> {
            sb.append(page.pageID).append(":").append(page.freeSpace).append(";");
        });
        return sb.toString();
    }

}
