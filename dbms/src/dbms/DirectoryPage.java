package dbms;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashSet;

/**
 * Created by Asima on 22.11.2015.
 */
public class DirectoryPage {

    final static int MAX_NUMBER_OF_PAGES = 682;
    HashSet<InfoPage> pages = new HashSet<>();
    int pageID;
    int nextPageID = DataBase.NULL_REF;

    /**
     * @param id
     * @throws IOException
     */
    public DirectoryPage(int id) {
        pageID = id;
    }
    
    public void saveDataOnDisk() throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(DataBase.PAGE_BYTE_SIZE);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        pages.stream().forEach((page) -> {
            bb.putInt(page.pageID);
            bb.putShort(page.freeSpace);
        });
        bb.position(DataBase.PAGE_BYTE_SIZE - 4);
        bb.putInt(nextPageID);
        RandomAccessFile raf = DataBase.raf;
        raf.seek(pageID * 1l * DataBase.PAGE_BYTE_SIZE);
        raf.write(bb.array());
    }

    public void loadDataFromDisk() throws IOException {
        byte[] data = new byte[DataBase.PAGE_BYTE_SIZE];
        RandomAccessFile raf = DataBase.raf;
        raf.seek(pageID * 1l * DataBase.PAGE_BYTE_SIZE);
        raf.read(data, 0, data.length);
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        for(int i = 0; i < MAX_NUMBER_OF_PAGES; i++) {
            int id = bb.getInt();
            short freeSpace = bb.getShort();
            if(id != 0 || freeSpace != 0) {
                pages.add(new InfoPage(id, pageID, freeSpace));
            }
        }
        nextPageID = bb.getInt();
    }

    public HashSet<InfoPage> getPages() {
        return pages;
    }
}
