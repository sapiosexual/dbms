package dbms;

/**
 * Created by Asima on 23.11.2015.
 */
public class DirectoryRecord {

    public short getFreeSpace() {
        return freeSpace;
    }

    public int getPageID() {
        return pageID;
    }

    private short freeSpace;
    private int pageID;

    public DirectoryRecord(int pageID, short freeSpace){
        this.freeSpace = freeSpace;
        this.pageID = pageID;
    }

}
