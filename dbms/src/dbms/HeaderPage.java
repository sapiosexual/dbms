package dbms;

import java.io.*;

/**
 * Created by Asima on 22.11.2015.
 */
public class HeaderPage  {

//    Map<String, Table> pageTables;
    int pageID;

    public HeaderPage(int id) throws IOException {
        this.pageID = id;
    }

    public void saveDataOnDisk(byte[] data) throws IOException {
        RandomAccessFile raf = DataBase.raf;
        raf.seek(pageID * 1l * DataBase.PAGE_BYTE_SIZE);
        raf.write(data);
    }

    public byte[] loadDataFromDisk() throws IOException {
        byte[] data = new byte[DataBase.PAGE_BYTE_SIZE];
        RandomAccessFile raf = DataBase.raf;
        raf.seek(pageID * 1l * DataBase.PAGE_BYTE_SIZE);
        raf.read(data, 0, data.length);
        return data;
    }
}
