package dbms;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * Created by Asima on 21.11.2015.
 */
public class InfoPage implements Comparable<InfoPage> {

    private static short initialFreeSpace = DataBase.PAGE_BYTE_SIZE - 4;
    public short freeSpace; //free space in bytes remained in page
    public int pageID;
    public int directoryID;

    //TODO: would be much better, if you'll store Records in List
    //private byte[] data; //whole page data
    private TreeMap<Integer, Short> slots;
    private TreeMap<Integer, Record> records;
    Map<String, AttributeType> attributes;

    public InfoPage(int pageID, int directoryID, short freeSpace) {
        this.pageID = pageID;
        this.directoryID = directoryID;
        this.freeSpace = freeSpace;
    }

    public InfoPage(int pageID, int directoryID) {
        this(pageID, directoryID, initialFreeSpace);
    }

    public InfoPage(short freeSpace) {
        this(DataBase.NULL_REF, DataBase.NULL_REF, freeSpace);
    }

    public void insert(Record rec) {
        int slot = getFreeSlot();
        int size = rec.size();
        if(slot >= slots.size()) {
            size +=2;
        }
        if(size > freeSpace)
            throw new RuntimeException("Not enough space for record");
        slots.put(slot, (short)DataBase.NULL_REF);
        records.put(slot, rec);
        freeSpace = countFreeSpace();
    }

    public void delete(int slot) {
        if(slots.containsKey(slot)) {
            slots.put(slot, (short)DataBase.NULL_REF);
            records.remove(slot);
            freeSpace = countFreeSpace();
        }
    }

    public boolean update(int slot, Record rec) {
        if(rec.size() < freeSpace + records.get(slot).size())
            return false;
        records.put(slot, rec);
        return true;
    }

    public Record getRecord(int slot) {
        return records.get(slot);
    }

    public Collection<Record> getRecords() {
        return records.values();
    }

    public int getFreeSlot() {
        for(Entry<Integer, Short> s : slots.entrySet()) {
            if(s.getValue() == DataBase.NULL_REF) {
                return s.getKey();
        }
    }
        return slots.size();
            }

    /**
     * Writes data in the file
     *
     * @throws IOException
     */
    public void saveDataOnDisk() throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(DataBase.PAGE_BYTE_SIZE);
        bb.putShort((short) slots.size());
        int currPosition = slots.size() * 2 + 2;
        for (Entry<Integer, Short> s : slots.entrySet()) {
            Record rec = records.get(s.getKey());
            if (rec != null) {
                bb.putShort((short) currPosition);
                currPosition += rec.writeRecord(bb.array(), currPosition);
            } else {
                bb.putShort((short) DataBase.NULL_REF);
            }
        }
        //save data
        RandomAccessFile raf = DataBase.raf;
        raf.seek(pageID * 1l * DataBase.PAGE_BYTE_SIZE);
        raf.write(bb.array());
        clear();
    }

    public void clear() {
        slots = null;
        records = null;
    }

    public void loadDataFromDisk(Map<String, AttributeType> attributes) throws IOException {
        this.attributes = attributes;
        loadDataFromDisk();
    }

    public void loadDataFromDisk() throws IOException {
        if (attributes == null) {
            throw new RuntimeException("Attributes are not initialized");
        }
        //Loading the whole page in RAM
        freeSpace = DataBase.PAGE_BYTE_SIZE - 2;
        slots = new TreeMap<>();
        records = new TreeMap<>();
        byte[] data = new byte[DataBase.PAGE_BYTE_SIZE];
        RandomAccessFile raf = DataBase.raf;  //TODO: Look for better method of random access, e.g. MappedByteBuffer
        raf.seek(pageID * 1l * DataBase.PAGE_BYTE_SIZE);
        raf.read(data, 0, data.length);
        //Loading metadata
        ByteBuffer bb = ByteBuffer.wrap(data);
        short numberOfSlots = bb.getShort();
        for (int i = 0; i < numberOfSlots; i++) {
            slots.put(i, bb.getShort());
        }
        slots.entrySet().stream().filter((s) -> (s.getValue() != DataBase.NULL_REF)).forEach((s) -> {
            Record rec = new Record(attributes, data, s.getValue());
            rec.setRid(new Rid(pageID, s.getKey().shortValue()));
            records.put(s.getKey(), rec);
        });
        freeSpace = countFreeSpace();
    }

    public short countFreeSpace() {
        int freeSpace = DataBase.PAGE_BYTE_SIZE - 2;
        freeSpace -= slots.size() * 2;
        freeSpace = records.values().stream().map((rec) -> rec.size())
                .reduce(freeSpace, (accumulator, _item) -> accumulator - _item);
        return (short) freeSpace;
    }

    @Override
    public int compareTo(InfoPage other) {
        int res = freeSpace - other.freeSpace;
        if (res == 0) {
            res = pageID - other.pageID;
        }
        return res;
    }

    @Override
    public int hashCode() {
        return pageID;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InfoPage other = (InfoPage) obj;
        return this.pageID == other.pageID;
    }
}
