package dbms;

import java.util.HashMap;
import java.util.Map;
import relationalalgebra.SelectionConditions;
import relationalalgebra.TupleSet;

/**
 *
 * @author Admin
 */
public class QueryProcessor {

    private static QueryProcessor instance = new QueryProcessor();
    private final String DELIMITER = ",";
    private final String COMMA = "&#44";

    private QueryProcessor() {

    }

    public static QueryProcessor getInstance() {
        return instance;
    }

    public String process(String query) {
        String[] tokens = query.split(DELIMITER);
        for(int i = 0; i < tokens.length; i++) {
            tokens[i] = tokens[i].trim();
        }
        String result = "Unknown";
        switch (tokens[0]) {
            case "insert_into_user":
                result = insertIntoUser(tokens);
                break;

            case "insert_into_affiliation":
                result = insertIntoAffiliation(tokens);
                break;

            case "insert_into_paper_affiliations":
                result = insertIntoPaperAffiliations(tokens);
                break;

            case "insert_into_key_word":
                result = insertIntoKeyWord(tokens);
                break;

            case "insert_into_paper_key_words":
                result = insertIntoPaperKeyWords(tokens);
                break;

            case "insert_into_author":
                result = insertIntoAuthor(tokens);
                break;

            case "insert_into_paper_authors":
                result = insertIntoPaperAuthors(tokens);
                break;

            case "insert_into_publisher":
                result = insertIntoPublisher(tokens);
                break;

            case "insert_into_publication":
                result = insertIntoPublication(tokens);
                break;

            case "insert_into_research_area":
                result = insertIntoResearchArea(tokens);
                break;

            case "insert_into_paper":
                result = insertIntoPaper(tokens);
                break;

            case "update_paper":
                result = updatePaper(tokens);
                break;

            case "update_paper_abstract":
                result = updatePaperAbstract(tokens);
                break;

            case "delete_paper_authors":
                result = deletePaperAuthors(tokens);
                break;

            case "delete_paper":
                result = deletePaper(tokens);
                break;

            case "delete_paper_key_words":
                result = deletePaperKeyWords(tokens);
                break;

            case "delete_paper_affiliations":
                result = deletePaperAffiliations(tokens);
                break;

            case "get_papers_by_publisher_user":
                result = getPapersByPublisherUser(tokens);
                break;

            case "get_papers_by_author_user":
                result = getPapersByAuthorUser(tokens);
                break;

            case "get_papers_by_title":
                result = getPapersByTitle(tokens);
                break;

            case "get_papers_by_author":
                result = getPapersByAuthor(tokens);
                break;

            case "get_papers_by_year":
                result = getPapersByYear(tokens);
                break;

            case "get_papers_by_research_area":
                result = getPapersByResearchArea(tokens);
                break;

            case "get_papers_by_affiliation":
                result = getPapersByAffiliation(tokens);
                break;

            case "get_papers_by_key_word":
                result = getPapersByKeyWords(tokens);
                break;

            case "get_paper_details":
                result = getPaperDetails(tokens);
                break;

            case "get_user_by_login":
                result = getUserByLogin(tokens);
                break;

            case "get_all":
                result = getAll(tokens);
                break;

            case "get_affiliations":
                result = getAffiliations(tokens);
                break;

            case "get_publisher":
                result = getPublisher(tokens);
                break;

            case "get_publication":
                result = getPublication(tokens);
                break;

            case "get_research_area":
                result = getResearchArea(tokens);
                break;

            case "get_authors":
                result = getAuthors(tokens);
                break;

            case "get_key_words":
                result = getKeyWords(tokens);
                break;
                
            case "get_user_id":
                result = getUserId(tokens);
                break;
                
            case "get_publisher_id":
                result = getPublisherId(tokens);
                break;
                
            case "get_publication_id":
                result = getPublicationId(tokens);
                break;
                
            case "get_research_area_id":
                result = getResearchAreaId(tokens);
                break;
                
            case "get_paper_id":
                result = getPaperId(tokens);
                break;
                
            case "get_key_word_id":
                result = getKeyWordId(tokens);
                break;
                
            case "get_author_id":
                result = getAuthorId(tokens);
                break;
                
            case "get_affiliation_id":
                result = getAffiliationId(tokens);
                break;
                
            case "get_affiliations_by_paper":
                result = getAffiliationsByPaper(tokens);
                break;
                
            case "get_key_words_by_paper":
                result = getKeyWordsByPaper(tokens);
                break;
                
            case "get_authors_by_paper":
                result = getAuthorsByPaper(tokens);
                break;

        }
        return result;
    }

    private String insertIntoUser(String[] tokens) {
        String res;
        try {
            String tableName = "user";
            String login = tokens[1];
            String hash = tokens[2];
            String type = tokens[3];
            int internal_id = Integer.parseInt(tokens[4]);

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setString("login", login);
            rec.setString("hash", hash);
            rec.setString("type", type);
            rec.setInteger("internal_id", internal_id);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoAffiliation(String[] tokens) {
        String res;
        try {
            String tableName = "affiliation";
            int id = Integer.parseInt(tokens[1]);
            String name = tokens[2];

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setString("name", name);
            rec.setInteger("id", id);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoPaperAffiliations(String[] tokens) {
        String res;
        try {
            String tableName = "paper_affiliations";
            int paper_id = Integer.parseInt(tokens[1]);
            int affiliation_id = Integer.parseInt(tokens[2]);

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setInteger("paper_id", paper_id);
            rec.setInteger("affiliation_id", affiliation_id);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoPublisher(String[] tokens) {
        String res;
        try {
            String tableName = "publisher";
            int id = Integer.parseInt(tokens[1]);
            String name = tokens[2];

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setString("name", name);
            rec.setInteger("id", id);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoPublication(String[] tokens) {
        String res;
        try {
            String tableName = "publication";
            int id = Integer.parseInt(tokens[1]);
            String issn = tokens[2];
            String name = tokens[3].replace(COMMA, ",");
            int year = Integer.parseInt(tokens[4]);
            int publisher_id = Integer.parseInt(tokens[5]);
            String isbn = tokens[6];
            String type = tokens[7];
            String volume = tokens[8];
            String issue = tokens[9];

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setInteger("id", id);
            rec.setInteger("year", year);
            rec.setInteger("publisher_id", publisher_id);
            rec.setString("issn", issn);
            rec.setString("name", name);
            rec.setString("isbn", isbn);
            rec.setString("type", type);
            rec.setString("volume", volume);
            rec.setString("issue", issue);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoResearchArea(String[] tokens) {
        String res;
        try {
            String tableName = "research_area";
            int id = Integer.parseInt(tokens[1]);
            String area = tokens[2];

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setString("area", area);
            rec.setInteger("id", id);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoPaper(String[] tokens) {
        String res;
        try {
            String tableName = "paper";
            int id = Integer.parseInt(tokens[1]);
            String title = tokens[2].replace(COMMA, ",");
            String link = tokens[3];
            String paperAbstract = tokens[4].replace(COMMA, ",");
            String doi = tokens[5];
            int research_area = Integer.parseInt(tokens[6]);
            int publication_id = Integer.parseInt(tokens[7]);
            String spage = tokens[8];
            String fpage = tokens[9];

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setInteger("id", id);
            rec.setString("year", paperAbstract);
            rec.setString("doi", doi);
            rec.setString("title", title);
            rec.setString("link", link);
            rec.setInteger("research_area", research_area);
            rec.setInteger("publication_id", publication_id);
            rec.setString("spage", spage);
            rec.setString("fpage", fpage);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoKeyWord(String[] tokens) {
        String res;
        try {
            String tableName = "key_word";
            int id = Integer.parseInt(tokens[1]);
            String term = tokens[2];

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setString("term", term);
            rec.setInteger("id", id);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoPaperKeyWords(String[] tokens) {
        String res;
        try {
            String tableName = "paper_key_words";
            int paper_id = Integer.parseInt(tokens[1]);
            int key_word_id = Integer.parseInt(tokens[2]);

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setInteger("paper_id", paper_id);
            rec.setInteger("key_word_id", key_word_id);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoAuthor(String[] tokens) {
        String res;
        try {
            String tableName = "author";
            int id = Integer.parseInt(tokens[1]);
            String name = tokens[2];

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setString("name", name);
            rec.setInteger("id", id);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }

        return res;
    }

    private String insertIntoPaperAuthors(String[] tokens) {
        String res;
        try {
            String tableName = "paper_authors";
            int paper_id = Integer.parseInt(tokens[1]);
            int key_word_id = Integer.parseInt(tokens[2]);
            int position = Integer.parseInt(tokens[3]);

            DataBase base = DataBase.getInstance();
            Map<String, AttributeType> attr = base.tables.get(tableName).attributes;
            Record rec = new Record(attr);
            rec.setInteger("paper_id", paper_id);
            rec.setInteger("key_word_id", key_word_id);
            rec.setInteger("position", position);

            base.insert(rec, tableName);
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String updatePaper(String[] tokens) {
        String res;
        try {
            String tableName = "paper";
            String[] params = new String[1];
            params[0] = tokens[1];
            DataBase base = DataBase.getInstance();
            TupleSet rows = base.tables.get(tableName)
                    .getRecords(SelectionConditions.EQ, "id", params);

            String title = tokens[2].replace(COMMA, ",");
            String link = tokens[3];
            String doi = tokens[4];
            if(tokens[5].equals("None")) tokens[5] = "0";
            int research_area_id = Integer.parseInt(tokens[5]);
            if(tokens[6].equals("None")) tokens[6] = "0";
            int publication_id = Integer.parseInt(tokens[6]);
            String spage = tokens[7];
            String fpage = tokens[8];

            for (Record rec : rows.getRecords()) {
                rec.setString("title", title);
                rec.setString("link", link);
                rec.setString("doi", doi);
                rec.setInteger("research_area_id", research_area_id);
                rec.setInteger("publication_id", publication_id);
                rec.setString("spage", spage);
                rec.setString("fpage", fpage);
                base.tables.get(tableName).update(rec.rid, rec);
            }
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String updatePaperAbstract(String[] tokens) {
        String res;
        try {
            String tableName = "paper";
            String[] params = new String[1];
            params[0] = tokens[1];
            DataBase base = DataBase.getInstance();
            TupleSet rows = base.tables.get(tableName)
                    .getRecords(SelectionConditions.EQ, "id", params);

            String paperAbstract = tokens[2].replace(COMMA, ",");

            for (Record rec : rows.getRecords()) {
                rec.setString("abstract", paperAbstract);
                base.tables.get(tableName).update(rec.rid, rec);
            }
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String deletePaper(String[] tokens) {
        String res;
        try {
            String tableName = "paper";
            String[] params = new String[1];
            params[0] = tokens[1];
            DataBase base = DataBase.getInstance();
            TupleSet rows = base.tables.get(tableName)
                    .getRecords(SelectionConditions.EQ, "id", params);

            for (Record rec : rows.getRecords()) {
                base.tables.get(tableName).delete(rec.rid);
            }
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String deletePaperAuthors(String[] tokens) {
        String res;
        try {
            String tableName = "paper_authors";
            String[] params = new String[1];
            params[0] = tokens[1];
            DataBase base = DataBase.getInstance();
            TupleSet rows = base.tables.get(tableName)
                    .getRecords(SelectionConditions.EQ, "paper_id", params);

            for (Record rec : rows.getRecords()) {
                base.tables.get(tableName).delete(rec.rid);
            }
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String deletePaperKeyWords(String[] tokens) {
        String res;
        try {
            String tableName = "paper_affiliations";
            String[] params = new String[1];
            params[0] = tokens[1];
            DataBase base = DataBase.getInstance();
            TupleSet rows = base.tables.get(tableName)
                    .getRecords(SelectionConditions.EQ, "paper_id", params);
            
            
            for(Record rec : rows.getRecords()) {
                base.tables.get(tableName).delete(rec.rid);
            }
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String deletePaperAffiliations(String[] tokens) {
        String res;
        try {
            String tableName = "paper_authors";
            String[] params = new String[1];
            params[0] = tokens[1];
            DataBase base = DataBase.getInstance();
            TupleSet rows = base.tables.get(tableName)
                    .getRecords(SelectionConditions.EQ, "paper_id", params);
            
            
            for(Record rec : rows.getRecords()) {
                base.tables.get(tableName).delete(rec.rid);
            }
            res = "Success";
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private TupleSet renameAndProject(TupleSet set) {
        String[] oldNames = new String[8];
        String[] newNames = new String[8];
        oldNames[0] = "p.id"; newNames[0] = "id";
        oldNames[1] = "p.title"; newNames[1] = "title";
        oldNames[2] = "p.doi"; newNames[2] = "doi";
        oldNames[3] = "p.spage"; newNames[3] = "spage";
        oldNames[4] = "p.fpage"; newNames[4] = "fpage";
        oldNames[5] = "pbln.issue"; newNames[5] = "issue";
        oldNames[6] = "pbln.volume"; newNames[6] = "volume";
        oldNames[7] = "pbln.year"; newNames[7] = "year";
        set = set.rename(oldNames, newNames);
        return set.projection(newNames);
    }
    
    private String mapSortingAttribute(String sorting_attribute) {
        if(sorting_attribute.equals("year")) return "pbln.year";
        if(sorting_attribute.equals("title")) return "p.title";
        return "p.title";
    }

    private String getPapersByPublisherUser(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            String[] params = new String[1];
            params[0] = tokens[2].toLowerCase();
            int limit = Integer.parseInt(tokens[3]);
            int offset = Integer.parseInt(tokens[4]);
            TupleSet user = base.tables.get("user")
                    .getRecords(SelectionConditions.EQ, "login", params);
            TupleSet publisher = base.tables.get("publisher").getRecords();
            user.order("internal_id");
            publisher.order("id");
            user.setAlias("u");
            publisher.setAlias("pblr");
            TupleSet resSet = publisher.join(user, "id", "internal_id");
            user = null;
            publisher = null;
            params = new String[resSet.size()];
            int i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("pblr.id");
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.IN, "publisher_id", params);
            publication.setAlias("pbln");
            publication.order("publisher_id");
            resSet.order("pblr.id");
            resSet = publication.join(resSet, "publisher_id", "pblr.id");
            publication = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("pbln.id");
            }
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.IN, "publication_id", params);
            paper.setAlias("p");
            paper.order("publication_id");
            resSet.order("pbln.id");
            resSet = paper.join(resSet, "publication_id", "pbln.id");
            paper = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = resSet.offsetAndLimit(offset, limit);
            resSet = renameAndProject(resSet);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");

            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPapersByAuthorUser(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            String[] params = new String[1];
            params[0] = tokens[2].toLowerCase();
            int limit = Integer.parseInt(tokens[3]);
            int offset = Integer.parseInt(tokens[4]);
            TupleSet user = base.tables.get("user")
                    .getRecords(SelectionConditions.EQ, "login", params);
            params = new String[user.size()];
            int i = 0;
            for(Record rec : user.getRecords()) {
                params[i++] = "" + rec.getInteger("internal_id");
            }
            TupleSet paperAuthors = base.tables.get("paper_authors")
                    .getRecords(SelectionConditions.IN, "author_id", params);
            user.order("internal_id");
            paperAuthors.order("author_id");
            user.setAlias("u");
            paperAuthors.setAlias("pau");
            TupleSet resSet = paperAuthors.join(user, "author_id", "internal_id");
            user = null;
            paperAuthors = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("pau.paper_id");
            }
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.IN, "id", params);
            paper.setAlias("p");
            paper.order("id");
            resSet.order("pau.paper_id");
            resSet = paper.join(resSet, "id", "pau.paper_id");
            paper = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("p.publication_id");
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.IN, "id", params);
            publication.setAlias("pbln");
            publication.order("id");
            resSet.order("p.publication_id");
            resSet = resSet.leftJoin(publication, "p.publication_id", "id");
            publication = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = resSet.offsetAndLimit(offset, limit);
            resSet = renameAndProject(resSet);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPapersByTitle(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            String[] params = new String[1];
            params[0] = tokens[2].toLowerCase();
            int limit = Integer.parseInt(tokens[3]);
            int offset = Integer.parseInt(tokens[4]);
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.LIKE, "title", params);
            params = new String[paper.size()];
            int i = 0;
            for(Record rec : paper.getRecords()) {
                params[i++] = "" + rec.getInteger("publication_id");
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.IN, "id", params);
            paper.setAlias("p");
            paper.order("publication_id");
            publication.setAlias("pbln");
            publication.order("id");
            TupleSet resSet = paper.leftJoin(publication, "publication_id", "id");
            paper = null;
            publication = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = resSet.offsetAndLimit(offset, limit);
            resSet = renameAndProject(resSet);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPapersByAuthor(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            String[] params = new String[1];
            params[0] = tokens[2].toLowerCase();
            int limit = Integer.parseInt(tokens[3]);
            int offset = Integer.parseInt(tokens[4]);
            TupleSet author = base.tables.get("author")
                    .getRecords(SelectionConditions.LIKE, "name", params);
            params = new String[author.size()];
            int i = 0;
            for(Record rec : author.getRecords()) {
                params[i++] = "" + rec.getInteger("id");
            }
            TupleSet paperAuthors = base.tables.get("paper_authors")
                    .getRecords(SelectionConditions.IN, "author_id", params);
            author.setAlias("au");
            author.order("id");
            paperAuthors.setAlias("pau");
            paperAuthors.order("author_id");
            TupleSet resSet = author.join(paperAuthors, "id", "author_id");
            author = null;
            paperAuthors = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("pau.paper_id");
            }
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.IN, "id", params);
            paper.setAlias("p");
            paper.order("id");
            resSet.order("pau.paper_id");
            resSet = paper.join(resSet, "id", "pau.paper_id");
            paper = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("p.publication_id");
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.IN, "id", params);
            publication.setAlias("pbln");
            publication.order("id");
            resSet.order("p.publication_id");
            resSet = resSet.leftJoin(publication, "p.publication_id", "id");
            publication = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = renameAndProject(resSet);
            resSet.distinct();
            resSet = resSet.offsetAndLimit(offset, limit);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPapersByYear(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            String[] params = new String[1];
            params[0] = tokens[2].toLowerCase();
            int limit = Integer.parseInt(tokens[3]);
            int offset = Integer.parseInt(tokens[4]);
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.EQ, "year", params);
            params = new String[publication.size()];
            int i = 0;
            for(Record rec : publication.getRecords()) {
                params[i++] = "" + rec.getInteger("id");
            }
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.IN, "publication_id", params);
            paper.setAlias("p");
            paper.order("publication_id");
            publication.setAlias("pbln");
            publication.order("id");
            TupleSet resSet = paper.join(publication, "publication_id", "id");
            paper = null;
            publication = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = resSet.offsetAndLimit(offset, limit);
            resSet = renameAndProject(resSet);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPapersByResearchArea(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            String[] params = new String[1];
            params[0] = tokens[2].toLowerCase();
            int limit = Integer.parseInt(tokens[3]);
            int offset = Integer.parseInt(tokens[4]);
            TupleSet researchArea = base.tables.get("research_area")
                    .getRecords(SelectionConditions.LIKE, "area", params);
            params = new String[researchArea.size()];
            int i = 0;
            for(Record rec : researchArea.getRecords()) {
                params[i++] = "" + rec.getInteger("id");
            }
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.IN, "research_area_id", params);
            researchArea.setAlias("ra");
            researchArea.order("id");
            paper.setAlias("p");
            paper.order("research_area_id");
            TupleSet resSet = paper.leftJoin(researchArea, "research_area_id", "id");
            researchArea = null;
            paper = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("p.publication");
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.IN, "id", params);
            publication.setAlias("pbln");
            publication.order("id");
            resSet.order("p.publication_id");
            resSet = resSet.leftJoin(publication, "p.publication_id", "id");
            publication = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = renameAndProject(resSet);
            resSet.distinct();
            resSet = resSet.offsetAndLimit(offset, limit);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPapersByAffiliation(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            String[] params = new String[1];
            params[0] = tokens[2].toLowerCase();
            int limit = Integer.parseInt(tokens[3]);
            int offset = Integer.parseInt(tokens[4]);
            TupleSet affiliation = base.tables.get("affiliation")
                    .getRecords(SelectionConditions.LIKE, "name", params);
            params = new String[affiliation.size()];
            int i = 0;
            for(Record rec : affiliation.getRecords()) {
                params[i++] = "" + rec.getInteger("id");
            }
            TupleSet paperAffiliations = base.tables.get("paper_affiliations")
                    .getRecords(SelectionConditions.IN, "affiliation_id", params);
            affiliation.setAlias("a");
            affiliation.order("id");
            paperAffiliations.setAlias("pa");
            paperAffiliations.order("affiliation_id");
            TupleSet resSet = affiliation.join(paperAffiliations, "id", "affiliation_id");
            affiliation = null;
            paperAffiliations = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("pa.paper_id");
            }
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.IN, "id", params);
            paper.setAlias("p");
            paper.order("id");
            resSet.order("pa.paper_id");
            resSet = paper.join(resSet, "id", "pa.paper_id");
            paper = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("p.publication_id");
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.IN, "id", params);
            publication.setAlias("pbln");
            publication.order("id");
            resSet.order("p.publication_id");
            resSet = resSet.leftJoin(publication, "p.publication_id", "id");
            publication = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = renameAndProject(resSet);
            resSet.distinct();
            resSet = resSet.offsetAndLimit(offset, limit);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPapersByKeyWords(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            String[] params = new String[1];
            params[0] = tokens[2].toLowerCase();
            int limit = Integer.parseInt(tokens[3]);
            int offset = Integer.parseInt(tokens[4]);
            TupleSet keyWord = base.tables.get("key_word")
                    .getRecords(SelectionConditions.LIKE, "term", params);
            params = new String[keyWord.size()];
            int i = 0;
            for(Record rec : keyWord.getRecords()) {
                params[i++] = "" + rec.getInteger("id");
            }
            TupleSet paperKeyWords = base.tables.get("paper_key_words")
                    .getRecords(SelectionConditions.IN, "key_word_id", params);
            keyWord.setAlias("kw");
            keyWord.order("id");
            paperKeyWords.setAlias("pkw");
            paperKeyWords.order("key_word_id");
            TupleSet resSet = keyWord.join(paperKeyWords, "id", "key_word_id");
            keyWord = null;
            paperKeyWords = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("pkw.paper_id");
            }
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.IN, "id", params);
            paper.setAlias("p");
            paper.order("id");
            resSet.order("pkw.paper_id");
            resSet = paper.join(resSet, "id", "pkw.paper_id");
            paper = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("p.publication_id");
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.IN, "id", params);
            publication.setAlias("pbln");
            publication.order("id");
            resSet.order("p.publication_id");
            resSet = resSet.leftJoin(publication, "p.publication_id", "id");
            publication = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = renameAndProject(resSet);
            resSet.distinct();
            resSet = resSet.offsetAndLimit(offset, limit);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPaperDetails(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[1];
            params[0] = tokens[1].toLowerCase();
            TupleSet paper = base.tables.get("paper")
                    .getRecords(SelectionConditions.EQ, "id", params);
            params = new String[paper.size()];
            int i = 0;
            for(Record rec : paper.getRecords()) {
                params[i++] = "" + rec.getInteger("research_area_id");
            }
            TupleSet researchArea = base.tables.get("research_area")
                    .getRecords(SelectionConditions.IN, "id", params);
            paper.setAlias("p");
            paper.order("research_area_id");
            researchArea.setAlias("ra");
            researchArea.order("id");
            TupleSet resSet = paper.leftJoin(researchArea, "research_area_id", "id");
            paper = null;
            researchArea = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("p.publication_id");
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.IN, "id", params);
            publication.setAlias("pbln");
            publication.order("id");
            resSet.order("p.publication_id");
            resSet = resSet.leftJoin(publication, "p.publication_id", "id");
            publication = null;
            params = new String[resSet.size()];
            i = 0;
            for(Record rec : resSet.getRecords()) {
                params[i++] = "" + rec.getInteger("pbln.publisher_id");
            }
            TupleSet publisher = base.tables.get("publisher")
                    .getRecords(SelectionConditions.IN, "id", params);
            publisher.setAlias("pblr");
            publisher.order("id");
            resSet.order("pbln.publisher_id");
            resSet = resSet.leftJoin(publisher, "pbln.publisher_id", "id");
            publisher = null;
            int count = resSet.size();
            String[] oldNames = new String[19];
            String[] newNames = new String[19];
            oldNames[0] = "p.id"; newNames[0] = "id";
            oldNames[1] = "p.title"; newNames[1] = "title";
            oldNames[2] = "p.link"; newNames[2] = "link";
            oldNames[3] = "p.abstract"; newNames[3] = "abstract";
            oldNames[4] = "p.doi"; newNames[4] = "doi";
            oldNames[5] = "p.spage"; newNames[5] = "spage";
            oldNames[6] = "p.fpage"; newNames[6] = "fpage";
            oldNames[7] = "ra.id"; newNames[7] = "research_area_id";
            oldNames[8] = "ra.area"; newNames[8] = "research_area";
            oldNames[9] = "pbln.id"; newNames[9] = "publication_id";
            oldNames[10] = "pbln.isbn"; newNames[10] = "isbn";
            oldNames[11] = "pbln.issn"; newNames[11] = "issn";
            oldNames[12] = "pbln.type"; newNames[12] = "pub_type";
            oldNames[13] = "pbln.name"; newNames[13] = "publication_name";
            oldNames[14] = "pbln.issue"; newNames[14] = "issue";
            oldNames[15] = "pbln.volume"; newNames[15] = "volume";
            oldNames[16] = "pbln.year"; newNames[16] = "year";
            oldNames[17] = "pblr.id"; newNames[17] = "publisher_id";
            oldNames[18] = "pblr.name"; newNames[18] = "publisher_name";
            resSet = resSet.rename(oldNames, newNames);
            resSet = resSet.projection(newNames);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getUserByLogin(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[1];
            params[0] = tokens[1].toLowerCase();
            TupleSet user = base.tables.get("user")
                    .getRecords(SelectionConditions.EQ, "login", params);
            int count = user.size();
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(user).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getAll(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String sorting_attribute = tokens[1];
            int limit = Integer.parseInt(tokens[2]);
            int offset = Integer.parseInt(tokens[3]);
            TupleSet paper = base.tables.get("paper")
                    .getRecords();
            paper.setAlias("p");
            paper.order("publication_id");
            TupleSet publication = base.tables.get("publication")
                    .getRecords();
            publication.setAlias("pbln");
            publication.order("id");
            TupleSet resSet = paper.leftJoin(publication, "publication_id", "id");
            paper = null;
            publication = null;
            resSet.order(mapSortingAttribute(sorting_attribute));
            int count = resSet.size();
            resSet = renameAndProject(resSet);
            resSet.distinct();
            resSet = resSet.offsetAndLimit(offset, limit);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getAffiliations(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].replace(COMMA, ",").toLowerCase();
            }
            TupleSet affiliation = base.tables.get("affiliation")
                    .getRecords(SelectionConditions.IN, "name", params);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(affiliation).append(", ").append(affiliation.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPublisher(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[1];
            params[0] = tokens[1].toLowerCase();
            TupleSet publisher = base.tables.get("publisher")
                    .getRecords(SelectionConditions.EQ, "name", params);
            int count = publisher.size();
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(publisher).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getPublication(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[1];
            params[0] = tokens[1].replace(COMMA, ",").toLowerCase();
            TupleSet publication = base.tables.get("publication")
                    .getRecords(SelectionConditions.EQ, "name", params);
            int count = publication.size();
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(publication).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getResearchArea(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[1];
            params[0] = tokens[1].toLowerCase();
            TupleSet researchArea = base.tables.get("research_area")
                    .getRecords(SelectionConditions.EQ, "area", params);
            int count = researchArea.size();
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(researchArea).append(", ").append(count).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getKeyWords(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet keyWord = base.tables.get("key_word")
                    .getRecords(SelectionConditions.IN, "term", params);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(keyWord).append(", ").append(keyWord.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }

    private String getAuthors(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet author = base.tables.get("author")
                    .getRecords(SelectionConditions.IN, "name", params);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(author).append(", ").append(author.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getUserId(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet user = base.tables.get("user")
                    .getRecords(SelectionConditions.EQ, "type", params);
            TupleSet max = user.max("internal_id");
            Record rec = max.getRecords().get(0);
            max.deleteRecord(rec);
            rec.setInteger("max", rec.getInteger("max") + 1);
            max.addRecord(rec);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(max).append(", ").append(max.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getPublisherId(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet publisher = base.tables.get("publisher")
                    .getRecords();
            TupleSet max = publisher.max("id");
            Record rec = max.getRecords().get(0);
            max.deleteRecord(rec);
            rec.setInteger("max", rec.getInteger("max") + 1);
            max.addRecord(rec);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(max).append(", ").append(max.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getPublicationId(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet publication = base.tables.get("publication")
                    .getRecords();
            TupleSet max = publication.max("id");
            Record rec = max.getRecords().get(0);
            max.deleteRecord(rec);
            rec.setInteger("max", rec.getInteger("max") + 1);
            max.addRecord(rec);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(max).append(", ").append(max.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getResearchAreaId(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet researchArea = base.tables.get("research_area")
                    .getRecords();
            TupleSet max = researchArea.max("id");
            Record rec = max.getRecords().get(0);
            max.deleteRecord(rec);
            rec.setInteger("max", rec.getInteger("max") + 1);
            max.addRecord(rec);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(max).append(", ").append(max.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getPaperId(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet paper = base.tables.get("paper")
                    .getRecords();
            TupleSet max = paper.max("id");
            Record rec = max.getRecords().get(0);
            max.deleteRecord(rec);
            rec.setInteger("max", rec.getInteger("max") + 1);
            max.addRecord(rec);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(max).append(", ").append(max.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getKeyWordId(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet keyWord = base.tables.get("key_word")
                    .getRecords();
            TupleSet max = keyWord.max("id");
            Record rec = max.getRecords().get(0);
            max.deleteRecord(rec);
            rec.setInteger("max", rec.getInteger("max") + 1);
            max.addRecord(rec);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(max).append(", ").append(max.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getAffiliationId(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet affiliation = base.tables.get("affiliation")
                    .getRecords();
            TupleSet max = affiliation.max("id");
            Record rec = max.getRecords().get(0);
            max.deleteRecord(rec);
            rec.setInteger("max", rec.getInteger("max") + 1);
            max.addRecord(rec);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(max).append(", ").append(max.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getAuthorId(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            String[] params = new String[tokens.length - 1];
            for(int i = 0 ; i < params.length; i++) {
                params[i] = tokens[i + 1].toLowerCase();
            }
            TupleSet author = base.tables.get("author")
                    .getRecords();
            TupleSet max = author.max("id");
            Record rec = max.getRecords().get(0);
            max.deleteRecord(rec);
            rec.setInteger("max", rec.getInteger("max") + 1);
            max.addRecord(rec);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(max).append(", ").append(max.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getAffiliationsByPaper(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            TupleSet paperAffiliations = base.tables.get("paper_affiliations")
                    .getRecords(SelectionConditions.EQ, "paper_id", tokens[1]);
            String[] params = new String[paperAffiliations.size()];
            int i = 0;
            for(Record rec : paperAffiliations.getRecords()) {
                params[i++] = "" + rec.getInteger("affiliation_id");
            }
            TupleSet affiliation = base.tables.get("affiliation")
                    .getRecords(SelectionConditions.IN, "id", params);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(affiliation).append(", ").append(affiliation.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getKeyWordsByPaper(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            TupleSet paperKeyWords = base.tables.get("paper_key_words")
                    .getRecords(SelectionConditions.EQ, "paper_id", tokens[1]);
            String[] params = new String[paperKeyWords.size()];
            int i = 0;
            for(Record rec : paperKeyWords.getRecords()) {
                params[i++] = "" + rec.getInteger("key_word_id");
            }
            TupleSet keyWords = base.tables.get("key_word")
                    .getRecords(SelectionConditions.IN, "id", params);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(keyWords).append(", ").append(keyWords.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
    
    private String getAuthorsByPaper(String[] tokens) {
        String res;
        try {
            DataBase base = DataBase.getInstance();
            TupleSet paperAuthors = base.tables.get("paper_authors")
                    .getRecords(SelectionConditions.EQ, "paper_id", tokens[1]);
            String[] params = new String[paperAuthors.size()];
            int i = 0;
            for(Record rec : paperAuthors.getRecords()) {
                params[i++] = "" + rec.getInteger("author_id");
            }
            TupleSet authors = base.tables.get("author")
                    .getRecords(SelectionConditions.IN, "id", params);
            authors.setAlias("au");
            authors.order("id");
            paperAuthors.setAlias("pau");
            paperAuthors.order("author_id");
            TupleSet resSet = authors.join(paperAuthors, "id", "author_id");
            resSet.order("pau.position");
            resSet = resSet.projection("au.name", "au.id");
            String[] oldNames = new String[2];
            String[] newNames = new String[2];
            oldNames[0] = "au.name"; newNames[0] = "name";
            oldNames[1] = "au.id"; newNames[1] = "id";
            resSet = resSet.rename(oldNames, newNames);
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(resSet).append(", ").append(resSet.size()).append("]");
            res = sb.toString();
        } catch (Exception e) {
            res = e.getMessage();
            e.printStackTrace();
        }
        return res;
    }
}
