package dbms;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Record {

    Map<String, AttributeType> attributes;
    Map<String, Object> values = new HashMap<>();
    int size = DataBase.NULL_REF;

    public void setRid(Rid rid) {
        this.rid = rid;
    }

    Rid rid;

    public Record(Map<String, AttributeType> attributes){
        this.attributes = attributes;
        initEmpty();
    }

    public Record(Map<String, AttributeType> attributes, byte[] bytes, int offset) {
        this.attributes = attributes;
        initFromByteArray(bytes, offset);
    }

    private void initEmpty() {
        attributes.entrySet().stream().forEach((attr) -> {
            switch(attr.getValue()) {
                case INTEGER:
                    values.put(attr.getKey(), 0);
                break;
                case STRING:
                    values.put(attr.getKey(), "");
                break;
            }
        });
    }

    private void initFromByteArray(byte[] bytes, int offset) {
        ByteBuffer buf = ByteBuffer.wrap(bytes, offset, bytes.length - offset);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        attributes.entrySet().stream().forEach((attr) -> {
            switch(attr.getValue()) {
                case INTEGER:
                    int value = buf.getInt();
                    values.put(attr.getKey(), value);
                break;

                case STRING:
                    int length = buf.getShort();
                    byte[] stringBytes = new byte[length];
                    buf.get(stringBytes);
                    values.put(attr.getKey(), new String(stringBytes));
                break;
            }
        });
    }
    
    public int writeRecord(byte[] bytes, int offset) {
        ByteBuffer buf = ByteBuffer.wrap(bytes, offset, bytes.length - offset);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        int size = 0;
        for(Entry<String, AttributeType> attr : attributes.entrySet()) {
            switch(attr.getValue()) {
                case INTEGER:
                    int value = (Integer)values.get(attr.getKey());
                    buf.putInt(value);
                    size += 4;
                break;
                
                case STRING:
                    byte[] stringBytes = ((String)values.get(attr.getKey())).getBytes();
                    int length = stringBytes.length;
                    buf.putShort((short)length);
                    buf.put(stringBytes);
                    size += 2 + length;
                break;
            }
        }
        return size;
    }
    
    public void setInteger(String attrName, int value) {
        setComparable(attrName, value);
    }
    
    public void setString(String attrName, String value) {
        setComparable(attrName, value);
    }
    
    public void setComparable(String attrName, Comparable value) {
        if(containsAttribute(attrName)) {
            values.put(attrName, value);
        } else {
            throw new IllegalArgumentException("There is no attribute with " + attrName + " name");
        }
    }
    
    public int getInteger(String attrName) {
        try {
            return (Integer)getComparable(attrName);
        } catch (ClassCastException e) {
            e.printStackTrace();
            throw new RuntimeException("Specified attribute is not integer");
        }
    }
    
    public String getString(String attrName) {
        try {
            return (String)getComparable(attrName);
        } catch (ClassCastException e) {
            e.printStackTrace();
            throw new RuntimeException("Specified attribute is not String");
        }
    }
    
    public Comparable getComparable(String attrName) {
        try {
            if(containsAttribute(attrName)) {
                return (Comparable)values.get(attrName);
            } else {
                throw new IllegalArgumentException("There is no attribute with " + attrName + " name");
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
            throw new RuntimeException("Specified attribute is not String");
        }
    }
    
    public boolean containsAttribute(String attrName) {
        return attributes.containsKey(attrName);
    }
    
    public int size() {
        if(size != DataBase.NULL_REF)
            return size;
        int newSize = 0;
        for(Entry<String, AttributeType> attr : attributes.entrySet()) {
            switch(attr.getValue()) {
                case INTEGER:
                    newSize += 4;
                break;
                
                case STRING:
                    newSize += 2 + ((String)values.get(attr.getKey())).getBytes().length;
                break;
            }
        }
        size = newSize;
        return newSize;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<Entry<String, AttributeType>> it = attributes.entrySet().iterator();
        while(it.hasNext()) {
            Entry<String, AttributeType> e = it.next();
            sb.append("'").append(e.getKey()).append("'").append(": ");
            switch(e.getValue()) {
                case STRING:
                    String value = (String)values.get(e.getKey());
                    value = value.replace("'", "\\'");
                    sb.append("'").append(value).append("'");
                break;
                
                case INTEGER:
                    sb.append(values.get(e.getKey()));
                break;
            }
            if(it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Record) {
            Record rec = (Record) obj;
            return values.equals(rec.values);
        }
        return false;
    }

    public Map<String, AttributeType> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, AttributeType> attributes) {
        this.attributes = attributes;
    }
    
    public void rename(String[] oldNames, String[] newNames) {
        for(int i = 0; i < oldNames.length; i++) {
            Object oldValue = values.remove(oldNames[i]);
            values.put(newNames[i], oldValue);
        }
    }
}
