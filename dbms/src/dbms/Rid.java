package dbms;

/**
 * Created by Asima on 21.11.2015.
 */
public class Rid {
    int pageNumber;
    short slot;

    public Rid(int pageNumber, short slot){
        this.pageNumber = pageNumber;
        this.slot = slot;
    }
}
