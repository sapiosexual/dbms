package dbms;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;
import relationalalgebra.SelectionConditions;
import relationalalgebra.TupleSet;

/**
 * Created by Asima on 22.11.2015.
 */
public class Table {

    Directory dir;
    Map<String, AttributeType> attributes;
    String name;

    public Table(String name, int dirRef, Map<String, AttributeType> attributes) {
        this.name = name;
        this.dir = new Directory(dirRef);
        this.attributes = attributes;
    }

    public Table(String name, int dirRef) {
        this(name, dirRef, new HashMap<>());
    }

    public Map<String, AttributeType> getAttributes() {
        return attributes;
    }

    public void update(Rid id, Record rec) throws IOException {
        InfoPage page = dir.pageIdIndex.get(id.pageNumber);
        page.loadDataFromDisk(attributes);
        dir.startReindex(page);
        if (!page.update(id.slot, rec)) {
            delete(id);
            insert(rec);
        } else {
            dir.endReindex(page);
            page.saveDataOnDisk();
        }
    }

    public void delete(Rid id) throws IOException {
        InfoPage page = dir.pageIdIndex.get(id.pageNumber);
        page.loadDataFromDisk(attributes);
        dir.startReindex(page);
        page.delete(id.slot);
        dir.endReindex(page);
        page.saveDataOnDisk();
    }

    public void insert(Record rec) throws IOException {
        if (fits(rec)) {
            InfoPage page = dir.getMostSuitablePage(rec.size());
            page.loadDataFromDisk(attributes);
            dir.startReindex(page);
            page.insert(rec);
            dir.endReindex(page);
            page.saveDataOnDisk();
        } else {
            throw new RuntimeException("wrong attributes set");
        }
    }

    private boolean fits(Record rec) {
        return rec.getAttributes().equals(attributes);
    }

    public TupleSet getRecords() throws IOException {
        TupleSet res = new TupleSet(attributes);
        for (InfoPage page : dir.spaceIndex) {
            page.loadDataFromDisk(attributes);
            page.getRecords().stream().forEach((rec) -> {
                res.addRecord(rec);
            });
            page.clear();
        }
        return res;
    }

    public TupleSet getRecords(SelectionConditions condition, String attrName, String... values) throws IOException {
        if (!attributes.containsKey(attrName)) {
            throw new IllegalArgumentException("There is no attribute with name " + attrName);
        }
        TupleSet res = new TupleSet(attributes);
        switch (attributes.get(attrName)) {
            case INTEGER:
                if (condition == SelectionConditions.LIKE) {
                    throw new UnsupportedOperationException("Like is not supported for integers");
                }
                Integer[] args = new Integer[values.length];
                for (int i = 0; i < values.length; i++) {
                    args[i] = Integer.parseInt(values[i]);
                }
                for (InfoPage page : dir.spaceIndex) {
                    page.loadDataFromDisk(attributes);
                    page.getRecords().stream().forEach((rec) -> {
                        if (condition.run(rec.getInteger(attrName), args)) {
                            res.addRecord(rec);
                        }
                    });
                    page.clear();
                }
                break;

            case STRING:
                for (InfoPage page : dir.spaceIndex) {
                    page.loadDataFromDisk(attributes);
                    page.getRecords().stream().forEach((rec) -> {
                        if (condition.run(rec.getString(attrName).toLowerCase(), values)) {
                            res.addRecord(rec);
                        }
                    });
                    page.clear();
                }
                break;
        }

        return res;
    }

    public byte[] getMetaData() {
        ByteBuffer bb = ByteBuffer.allocate(DataBase.PAGE_BYTE_SIZE - 5);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        byte[] nameBytes = name.getBytes();
        bb.putShort((short) nameBytes.length);
        bb.put(nameBytes);
        bb.putInt(dir.dirRef);
        bb.put((byte) attributes.size());
        attributes.entrySet().stream().forEach((e) -> {
            byte[] attrNameBytes = e.getKey().getBytes();
            bb.putShort((short) attrNameBytes.length);
            bb.put(attrNameBytes);
            bb.put(e.getValue().getType());
        });
        byte[] meta = new byte[bb.position()];
        byte[] bbArray = bb.array();
        System.arraycopy(bbArray, 0, meta, 0, meta.length);
        return meta;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(";").append(dir.dirRef).append(";");
        attributes.entrySet().stream().forEach((e) -> {
            sb.append(e.getKey()).append(":").append(e.getValue()).append(";");
        });
        return sb.toString();
    }
}
