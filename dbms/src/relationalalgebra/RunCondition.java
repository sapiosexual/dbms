package relationalalgebra;

import java.util.function.Predicate;

/**
 *
 * @author Admin
 */
@FunctionalInterface
public interface RunCondition {
    boolean run(Comparable value, Comparable... args);
}
