package relationalalgebra;

/**
 *
 * @author Admin
 */
public enum SelectionConditions {
    GTE((value, args) -> {
        return value.compareTo(args[0]) >= 0;
    }),
    GT((value, args) -> {return value.compareTo(args[0]) > 0;}),
    LTE((value, args) -> {return value.compareTo(args[0]) <= 0;}),
    LT((value, args) -> {return value.compareTo(args[0]) < 0;}),
    EQ((value, args) -> {return value.compareTo(args[0]) == 0;}),
    NEQ((value, args) -> {return value.compareTo(args[0]) != 0;}),
    LIKE((value, args) -> {
        String s = (String)value;
        return s.contains((String)args[0]);
    }),
    IN((value, args) -> {
        for(Comparable arg : args) {
            if(value.compareTo(arg) == 0) return true;
        }
        return false;
    });
    
    private final RunCondition runCondition;

    private SelectionConditions(RunCondition runCondition) {
        this.runCondition = runCondition;
    }
    
    public boolean run(Comparable value, Comparable... args) {
        return runCondition.run(value, args);
    }
}
