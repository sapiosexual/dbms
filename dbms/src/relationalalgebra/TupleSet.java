package relationalalgebra;

import dbms.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 *
 * @author Admin
 */
public class TupleSet {

    List<Record> records = new ArrayList<>();
    Map<String, AttributeType> attributes = new HashMap<>();
    String alias;

    public TupleSet(Map<String, AttributeType> attributes) {
        this.attributes.putAll(attributes);
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }

    public List<Record> getRecords() {
        return records;
    }

    public boolean addRecord(Record rec) {
        if (fits(rec)) {
            rec.setAttributes(attributes);
            records.add(rec);
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteRecord(Record rec) {
        return records.remove(rec);
    }

    private boolean fits(Record rec) {
        return rec.getAttributes().equals(attributes);
    }

    public TupleSet selection(SelectionConditions condition, String attrName, String... values) {
        if (!attributes.containsKey(attrName)) {
            throw new IllegalArgumentException("There is no attribute with name " + attrName);
        }
        if (records.isEmpty()) {
            return new TupleSet(attributes);
        }
        TupleSet result = new TupleSet(attributes);
        switch (attributes.get(attrName)) {
            case INTEGER:
                if (condition == SelectionConditions.LIKE) {
                    throw new UnsupportedOperationException("Like is not supported for integers");
                }
                Integer[] args = new Integer[values.length];
                for (int i = 0; i < values.length; i++) {
                    args[i] = Integer.parseInt(values[i]);
                }
                for (Record rec : records) {
                    if (condition.run(rec.getInteger(attrName), args)) {
                        result.addRecord(rec);
                    }
                }
                break;

            case STRING:
                for (Record rec : records) {
                    if (condition.run(rec.getString(attrName).toLowerCase(), values)) {
                        result.addRecord(rec);
                    }
                }
                break;
        }

        return result;
    }

    public TupleSet projection(String... newAttributes) {
        List<String> unexistedAttributes = unexistedAttributes(newAttributes);
        if (!unexistedAttributes.isEmpty()) {
            throw new IllegalArgumentException("One or more specified attributes don't exist: " + unexistedAttributes);
        }
        Map<String, AttributeType> attrs = new HashMap<>();
        for (String attr : newAttributes) {
            attrs.put(attr, attributes.get(attr));
        }
        TupleSet res = new TupleSet(attrs);
        records.stream().forEach((rec) -> {
            Record newRec = new Record(attrs);
            attrs.entrySet().stream().forEach((attr) -> {
                switch (attr.getValue()) {
                    case INTEGER:
                        newRec.setInteger(attr.getKey(), rec.getInteger(attr.getKey()));
                        break;

                    case STRING:
                        newRec.setString(attr.getKey(), rec.getString(attr.getKey()));
                        break;
                }
            });
            res.addRecord(newRec);
        });
        return res;
    }

    private List<String> unexistedAttributes(String... newAttributes) {
        List<String> unexistedAttributes = new ArrayList<>();
        for (String s : newAttributes) {
            if (!attributes.containsKey(s)) {
                unexistedAttributes.add(s);
            }
        }
        return unexistedAttributes;
    }

    public TupleSet order(final String attrName) {
        if (!attributes.containsKey(attrName)) {
            throw new IllegalArgumentException("There is no attribute with name " + attrName);
        }
        if (records.isEmpty()) {
            return new TupleSet(attributes);
        }
        Collections.sort(records, (rec1, rec2) -> {
            return rec1.getComparable(attrName).compareTo(rec2.getComparable(attrName));
        });
        return this;
    }

    public TupleSet rename(String[] oldNames, String[] newNames) {
        List<String> unexistedAttributes = unexistedAttributes(oldNames);
        if (!unexistedAttributes.isEmpty()) {
            throw new IllegalArgumentException("One or more specified attributes don't exist: " + unexistedAttributes);
        }
        if (oldNames.length != newNames.length) {
            throw new IllegalArgumentException("There are different numbers of attribute to rename: " + unexistedAttributes);
        }
        for (int i = 0; i < oldNames.length; i++) {
            AttributeType old = attributes.remove(oldNames[i]);
            attributes.put(newNames[i], old);
        }
        records.stream().forEach((rec) -> {
            rec.rename(oldNames, newNames);
        });
        return this;
    }

    /**
     * VERY IMPORTANT Sets should be sorted by attrName1 and attrName2
     *
     * @param joinable
     * @param attrName1
     * @param attrName2
     * @return
     */
    public TupleSet leftJoin(TupleSet joinable, String attrName1, String attrName2) {
        if (!attributes.containsKey(attrName1)) {
            throw new IllegalArgumentException("Attribute " + attrName1 + " doesn't exist");
        }
        if (!joinable.attributes.containsKey(attrName2)) {
            throw new IllegalArgumentException("Attribute " + attrName2 + " doesn't exist");
        }
        if (!attributes.get(attrName1).equals(joinable.attributes.get(attrName2))) {
            throw new IllegalArgumentException("Different types");
        }

        Map<String, AttributeType> attrs = new HashMap<>();
        attributes.entrySet().stream().forEach((e) -> {
            if(alias == null) {
                attrs.put(e.getKey(), e.getValue());
            } else {
                attrs.put(alias + "." + e.getKey(), e.getValue());
            }
        });
        joinable.attributes.entrySet().stream().forEach((e) -> {
            if(joinable.alias == null) {
                attrs.put(e.getKey(), e.getValue());
            } else {
                attrs.put(joinable.alias + "." + e.getKey(), e.getValue());
            }
        });
        TupleSet result = new TupleSet(attrs);
        int firstSetSize = records.size();
        int secondSetSize = joinable.records.size();
        int firstSetPointer = 0;
        int secondSetPointer = 0;
        while (firstSetPointer < firstSetSize
                && secondSetPointer < secondSetSize) {
            Record rec1 = records.get(firstSetPointer);
            Record rec2 = joinable.records.get(secondSetPointer);
            int cond = rec1.getComparable(attrName1).compareTo(rec2.getComparable(attrName2));
            if (cond > 0) {
                secondSetPointer++;
            } else if (cond == 0) {
                Comparable value = rec1.getComparable(attrName1);
                List<Record> firstCart = new ArrayList<>();
                do {
                    firstCart.add(records.get(firstSetPointer));
                    firstSetPointer++;
                } while (firstSetPointer < firstSetSize
                        && records.get(firstSetPointer).getComparable(attrName1).compareTo(value) == 0);
                List<Record> secondCart = new ArrayList<>();
                do {
                    secondCart.add(joinable.records.get(secondSetPointer));
                    secondSetPointer++;
                } while (secondSetPointer < secondSetSize
                        && joinable.records.get(secondSetPointer).getComparable(attrName2).compareTo(value) == 0);
                firstCart.stream().forEach((cartRec1) -> {
                    secondCart.stream().forEach((cartRec2) -> {
                        Record newRecord = new Record(attrs);
                        attributes.entrySet().stream().forEach((e) -> {
                            String key = alias == null? e.getKey(): alias + "." + e.getKey();
                            newRecord.setComparable(key, cartRec1.getComparable(e.getKey()));
                        });
                        joinable.attributes.entrySet().stream().forEach((e) -> {
                            String key = joinable.alias == null? e.getKey(): joinable.alias + "." + e.getKey();
                            newRecord.setComparable(key, cartRec2.getComparable(e.getKey()));
                        });
                        result.addRecord(newRecord);
                    });
                });
            } else {
                if (secondSetPointer == 0
                        || rec1.getComparable(attrName1).compareTo(joinable.records.get(secondSetPointer - 1).getComparable(attrName2)) > 0) {
                    Record newRecord = new Record(attrs);
                    attributes.entrySet().stream().forEach((e) -> {
                        String key = alias == null? e.getKey(): alias + "." + e.getKey();
                        newRecord.setComparable(key, rec1.getComparable(e.getKey()));
                    });
                    result.addRecord(newRecord);
                }
                firstSetPointer++;
            }
        }
        if(secondSetSize == 0) {
            records.stream().forEach((rec) -> {
                Record newRecord = new Record(attrs);
                attributes.entrySet().stream().forEach((e) -> {
                    String key = alias == null? e.getKey(): alias + "." + e.getKey();
                    newRecord.setComparable(key, rec.getComparable(e.getKey()));
                });
                result.addRecord(newRecord);
            });
        }
        return result;
    }
    
    public TupleSet join(TupleSet joinable, String attrName1, String attrName2) {
        if (!attributes.containsKey(attrName1)) {
            throw new IllegalArgumentException("Attribute " + attrName1 + " doesn't exist");
        }
        if (!joinable.attributes.containsKey(attrName2)) {
            throw new IllegalArgumentException("Attribute " + attrName2 + " doesn't exist");
        }
        if (!attributes.get(attrName1).equals(joinable.attributes.get(attrName2))) {
            throw new IllegalArgumentException("Different types");
        }

        Map<String, AttributeType> attrs = new HashMap<>();
        attributes.entrySet().stream().forEach((e) -> {
            if(alias == null) {
                attrs.put(e.getKey(), e.getValue());
            } else {
                attrs.put(alias + "." + e.getKey(), e.getValue());
            }
        });
        joinable.attributes.entrySet().stream().forEach((e) -> {
            if(joinable.alias == null) {
                attrs.put(e.getKey(), e.getValue());
            } else {
                attrs.put(joinable.alias + "." + e.getKey(), e.getValue());
            }
        });
        TupleSet result = new TupleSet(attrs);
        int firstSetSize = records.size();
        int secondSetSize = joinable.records.size();
        int firstSetPointer = 0;
        int secondSetPointer = 0;
        while (firstSetPointer < firstSetSize
                && secondSetPointer < secondSetSize) {
            Record rec1 = records.get(firstSetPointer);
            Record rec2 = joinable.records.get(secondSetPointer);
            int cond = rec1.getComparable(attrName1).compareTo(rec2.getComparable(attrName2));
            if (cond > 0) {
                secondSetPointer++;
            } else if (cond == 0) {
                Comparable value = rec1.getComparable(attrName1);
                List<Record> firstCart = new ArrayList<>();
                do {
                    firstCart.add(records.get(firstSetPointer));
                    firstSetPointer++;
                } while (firstSetPointer < firstSetSize
                        && records.get(firstSetPointer).getComparable(attrName1).compareTo(value) == 0);
                List<Record> secondCart = new ArrayList<>();
                do {
                    secondCart.add(joinable.records.get(secondSetPointer));
                    secondSetPointer++;
                } while (secondSetPointer < secondSetSize
                        && joinable.records.get(secondSetPointer).getComparable(attrName2).compareTo(value) == 0);
                firstCart.stream().forEach((cartRec1) -> {
                    secondCart.stream().forEach((cartRec2) -> {
                        Record newRecord = new Record(attrs);
                        attributes.entrySet().stream().forEach((e) -> {
                            String key = alias == null? e.getKey(): alias + "." + e.getKey();
                            newRecord.setComparable(key, cartRec1.getComparable(e.getKey()));
                        });
                        joinable.attributes.entrySet().stream().forEach((e) -> {
                            String key = joinable.alias == null? e.getKey(): joinable.alias + "." + e.getKey();
                            newRecord.setComparable(key, cartRec2.getComparable(e.getKey()));
                        });
                        result.addRecord(newRecord);
                    });
                });
            } else {
                firstSetPointer++;
            }
        }
        return result;
    }

    public TupleSet offsetAndLimit(int offset, int limit) {
        if (offset < records.size()) {
            TupleSet res = new TupleSet(attributes);
            int topBound = Integer.min(offset + limit, records.size());
            for (int i = offset; i < topBound; i++) {
                res.addRecord(records.get(i));
            }
            return res;
        } else {
            return new TupleSet(attributes);
        }
    }

    public TupleSet count() {
        Map<String, AttributeType> attr = new HashMap<>();
        attr.put("count", AttributeType.INTEGER);
        TupleSet res = new TupleSet(attr);
        Record rec = new Record(attr);
        rec.setInteger("count", records.size());
        return res;
    }

    public TupleSet max(String attrName) {
        Optional<Record> maxOptional = records.stream().max((rec1, rec2) -> {
            return rec1.getComparable(attrName).compareTo(rec2.getComparable(attrName));
        });
        Comparable max;
        switch(attributes.get(attrName)) {
            case INTEGER:
                max = 1;
                break;
            case STRING:
                max = "";
                break;
            default:
                max = 1;
                break;
        }
        if(maxOptional.isPresent()) {
            max = maxOptional.get().getComparable(attrName);
        }
        Map<String, AttributeType> attr = new HashMap<>();
        attr.put("max", attributes.get(attrName));
        TupleSet res = new TupleSet(attr);
        Record rec = new Record(attr);
        rec.setComparable("max", max);
        res.addRecord(rec);
        return res;
    }
    
    public TupleSet distinct() {
        Set<Record> set = new HashSet<>();
        records.stream().forEach((rec) -> {
            set.add(rec);
        });
        records.clear();
        records.addAll(set);
        return this;
    }
    
    public int size() {
        return records.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator<Record> it = records.iterator();
        sb.append("[");
        while(it.hasNext()) {
            Record rec = it.next();
            sb.append(rec);
            if(it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
